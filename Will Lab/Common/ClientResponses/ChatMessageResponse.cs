﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;using Common.ClientResponses;
using Common.Enums;

namespace Common.ClientResponses
{
    [Serializable]
    public class ChatMessageResponse : IResponse
    {
        public ResponseType GetResponseType()
        {
            return ResponseType.ChatMessage;
        }

        public string Sender { get; set; }

        public string Message { get; set; }

        public bool ServerMessage { get; set; } = false;

        public ChatMessageResponse(string sender, string message)
        {
            Sender = sender;
            Message = message;
        }
    }
}
