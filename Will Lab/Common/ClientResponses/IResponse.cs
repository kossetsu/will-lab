﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Enums;

namespace Common.ClientResponses
{
    public interface IResponse
    {
        ResponseType GetResponseType();
    }
}
