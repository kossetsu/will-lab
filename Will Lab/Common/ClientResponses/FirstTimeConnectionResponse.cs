﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Enums;

namespace Common.ClientResponses
{
    [Serializable]
    public class FirstTimeConnectionResponse : IResponse
    {
        public ResponseType GetResponseType()
        {
            return ResponseType.FirstTimeConnection;
        }

        public bool ConnectionAccepted { get; set; }

        public FirstTimeConnectionResponse(bool connectionAccepted)
        {
            ConnectionAccepted = connectionAccepted;
        }
    }
}
