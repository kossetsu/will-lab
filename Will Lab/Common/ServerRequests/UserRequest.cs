﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Enums;

namespace Common.ServerRequests
{
    [Serializable]
    public class UserRequest : IRequest
    {
        public RequestType GetRequestType()
        {
            return RequestType.User;
        }

        public ClientType ClientType { get; set; }

        public string Name { get; set; }

        public UserRequest(string name, ClientType clientType)
        {
            ClientType = clientType;
            Name = name;
        }
    }
}
