﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Enums;

namespace Common.ServerRequests
{
    [Serializable]
    public class ChatMessageRequest : IRequest
    {
        public RequestType GetRequestType()
        {
            return RequestType.ChatMessage;
        }

        public string Message { get; set; }

        public ChatMessageRequest(string message)
        {
            Message = message;
        }
    }
}
