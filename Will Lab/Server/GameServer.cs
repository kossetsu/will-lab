﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using Common.ClientResponses;
using Common.Enums;
using Common.ServerRequests;
using log4net;

namespace Server
{
    class GameServer
    {
        private TcpListener _listener;

        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        // Clients
        private readonly List<UserClient> _userClients = new List<UserClient>();

        public readonly string GameName;
        public readonly int Port;
        public bool Running { get; private set; }

        public readonly int BufferSize = 20 * 1024; // 20KB Buffer

        public GameServer(string gameName, int port)
        {
            GameName = gameName;
            Port = port;
            Running = false;

            _listener = new TcpListener(IPAddress.Any, Port);
        }

        // Stop the server running.
        public void Shutdown()
        {
            Running = false;
            log.InfoFormat("Good game! Server going offline.");
        }

        public void Run()
        {
            log.InfoFormat("Starting the \"{0}\" Game Server on port {1}.", GameName, Port);
            log.Info("Press Ctrl+C To shut down the server.");

            // Run server
            _listener.Start();
            Running = true;

            while (Running)
            {
                if (_listener.Pending())
                {
                    _handleNewConnection();
                }

                _checkForDisconnects();
                _checkForNewRequests();
                _dispatchResponses();

                Thread.Sleep(10);
            }

            foreach (UserClient client in _userClients)
            {
                client.CleanupTcpClient();
            }

            _listener.Stop();

            // Some info
            log.Info("Server is shut down.");
        }

        private void _handleNewConnection()
        {
            bool good = false;
            UserClient newUserClient = new UserClient();
            TcpClient newClient = _listener.AcceptTcpClient();
            NetworkStream netStream = newClient.GetStream();

            newClient.SendBufferSize = BufferSize;
            newClient.ReceiveBufferSize = BufferSize;

            EndPoint endPoint = newClient.Client.RemoteEndPoint;
            log.InfoFormat("New client connecting from {0}...", endPoint);

            BinaryFormatter formatter = new BinaryFormatter();
        
            var request = (UserRequest) formatter.Deserialize(netStream);

            log.DebugFormat("Got request of type {0}.", request.GetRequestType());

            if (_userClients.All(c => c.Name != request.Name))
            {
                if (request.ClientType == ClientType.Player &&
                    _userClients.Count(c => c.Type == ClientType.Player) > 2)
                {
                    // do nothing, is not good
                }
                else
                {
                    newUserClient.Type = request.ClientType;
                    newUserClient.Name = request.Name;
                    newUserClient.TcpClient = newClient;
                    _userClients.Add(newUserClient);
                    good = true;
                }
            }

            if (!good)
            {
                Console.WriteLine("Bad connection from {0}.", endPoint);
                newClient.Close();
            }
            else
            {
                _sendServerMessage(String.Format("{0} joined the game as a {1}.", request.Name, request.ClientType));
                log.InfoFormat("User {0} joined as a {1}.", request.Name, request.ClientType);

                var response = new FirstTimeConnectionResponse(true);

                formatter.Serialize(netStream, response);
            }
        }

        private void _sendServerMessage(string msg)
        {
            var request = new ChatMessageRequest(msg);
            _handleChatRequest(request, null);
        }

        private void _checkForDisconnects()
        {
            var clientsToRemove = new List<UserClient>();

            foreach (UserClient client in _userClients)
            {
                if (client.Disconnected())
                {
                    log.InfoFormat("{0} {1} has left.", client.Type, client.Name);

                    // cleanup
                    client.CleanupTcpClient();
                    clientsToRemove.Add(client);
                }
            }

            foreach (var client in clientsToRemove)
            {
                _userClients.Remove(client);
            }
        }

        private void _checkForNewRequests()
        {
            foreach (var client in _userClients)
            {
                var tcpClient = client.TcpClient;

                if (tcpClient.Available > 0)
                {
                    BinaryFormatter formatter = new BinaryFormatter();
                    var netStream = tcpClient.GetStream();
                    var request = (IRequest) formatter.Deserialize(netStream);
                    _handleRequest(request, client);
                }
            }
        }

        private void _dispatchResponses()
        {
            foreach (var client in _userClients)
            {
                client.DispatchMessages();
            }
        }

        private void _handleRequest(IRequest request, UserClient client)
        {
            switch (request.GetRequestType())
            {
                case RequestType.ChatMessage:
                    _handleChatRequest((ChatMessageRequest) request, client);
                    break;
            }
        }

        // Request handlers
        private void _handleChatRequest(ChatMessageRequest request, UserClient client)
        {

            // form response
            ChatMessageResponse response;

            // if client is null, message is from the server
            if (client == null)
            {
                response = new ChatMessageResponse("Server", request.Message);
                response.ServerMessage = true;
                log.InfoFormat("<SERVER> {0}", request.Message);
            }
            else
            {
                log.InfoFormat("{0}: {1}", client.Name, request.Message);
                response = new ChatMessageResponse(client.Name, request.Message);
            }

            foreach (var c in _userClients)
            {
                c.AddMessageToQueue(response);
            }
        }
    }
}
