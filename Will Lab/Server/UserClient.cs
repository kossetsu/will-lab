﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using Common.ClientResponses;
using Common.Enums;
using log4net;

namespace Server
{
    public class UserClient
    {
        // logger
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public string Name { get; set; }
        public ClientType Type { get; set; }

        public TcpClient TcpClient { get; set; }

        private List<IResponse> _messageQueue;

        private BinaryFormatter _formatter;

        public UserClient()
        {
            _messageQueue = new List<IResponse>();
            _formatter = new BinaryFormatter();
        }

        public void AddMessageToQueue(IResponse response)
        {
            _messageQueue.Add(response);
        }

        public void DispatchMessages()
        {
            foreach (var response in _messageQueue)
            {
                log.DebugFormat("Dispatching event {0} to {1}.", response.GetResponseType(), Name);

                var netStream = TcpClient.GetStream();
                
                _formatter.Serialize(netStream, response);
            }

            _messageQueue.Clear();
        }

        // Checks if a socket has disconnected
        // Adapted from -- http://stackoverflow.com/questions/722240/instantly-detect-client-disconnection-from-server-socket
        public bool Disconnected()
        {
            try
            {
                Socket s = TcpClient.Client;
                return s.Poll(10 * 1000, SelectMode.SelectRead) && (s.Available == 0);
            }
            catch (SocketException se)
            {
                return true;
            }
        }

        public void CleanupTcpClient()
        {
            TcpClient.GetStream().Close();     // Close network stream
            TcpClient.Close();                 // Close client
        }
    }
}
