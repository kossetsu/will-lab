﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;

namespace Server
{
    class Program
    {
        public static GameServer gameServer;

        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        protected static void InterruptHandler(object sender, ConsoleCancelEventArgs args)
        {
            gameServer.Shutdown();
            args.Cancel = true;
        }

        static void Main(string[] args)
        {
            string name = "Good Game!";
            int port = 6000;
            gameServer = new GameServer(name, port);

            Console.CancelKeyPress += InterruptHandler;

            gameServer.Run();
        }
    }
}
