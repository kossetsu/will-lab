﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.ServerRequests;
using Prism.Events;

namespace WillLab.Events
{
    public class SendRequestEvent : PubSubEvent<IRequest>
    {
    }
}
