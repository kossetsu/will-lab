﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using Common.Enums;
using log4net;
using MahApps.Metro.Controls.Dialogs;
using Prism.Commands;
using Prism.Mvvm;
using Unity;
using WillLab.Managers;
using WillLab.Views;

namespace WillLab.ViewModels
{
    public class ConnectionViewModel : BindableBase
    {
        // logger
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        // view elements
        private ClientType _selectedClientType;
        private string _serverAddress;
        private int _serverPort;
        private IDialogCoordinator _dialogCoordinator;
        private bool _isBusy;

        public string ClientName;
        public IConfigManager ConfigManager;

        // commands
        public DelegateCommand<ConnectionView> ConnectCommand { get; private set; }
        public DelegateCommand OpenConfigCommand { get; private set; } 

        // other properties
        private bool _connected;

        // connection
        public TcpClient Client;
        public NetworkStream NetStream;
        public readonly int BufferSize = 20 * 1024; // 20KB buffer

        public ClientType SelectedClientType
        {
            get { return _selectedClientType; }
            set
            {
                SetProperty(ref _selectedClientType, value);
                ConnectCommand.RaiseCanExecuteChanged();
            }
        }

        public string ServerAddress
        {
            get { return _serverAddress; }
            set
            {
                SetProperty(ref _serverAddress, value);
                ConnectCommand.RaiseCanExecuteChanged();
            }
        }

        public bool Connected
        {
            get { return _connected; }
            set { SetProperty(ref _connected, value); }
        }

        public int ServerPort
        {
            get { return _serverPort; }
            set
            {
                SetProperty(ref _serverPort, value);
                ConnectCommand.RaiseCanExecuteChanged();
            }
        }

        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                SetProperty(ref _isBusy, value);
                ConnectCommand.RaiseCanExecuteChanged();
                OpenConfigCommand.RaiseCanExecuteChanged();
            }
        }

        // end of properties

        public ConnectionViewModel(IDialogCoordinator instance)
        {
            Connected = false;
            ConnectCommand = new DelegateCommand<ConnectionView>(ExecuteConnect, CanConnect);
            OpenConfigCommand = new DelegateCommand(ExecuteOpenConfig, CanOpenConfig);
            Client = new TcpClient();
            _dialogCoordinator = instance;

            ServerAddress = "localhost";
            ServerPort = 6000;
        }

        private ConnectionView tempWindow;

        private void ExecuteConnect(ConnectionView window)
        {
            tempWindow = window;
            new Thread(new ThreadStart(CreateConnection)).Start();
        }

        private bool CanConnect(ConnectionView window)
        {
            return !string.IsNullOrEmpty(ServerAddress) &&
                   !IsBusy &&
                   ServerPort > 0;
        }

        private void ExecuteOpenConfig()
        {
            var configWindow = new ConfigView(ConfigManager);

            configWindow.ShowDialog();
        }

        private bool CanOpenConfig()
        {
            return !IsBusy;
        }

        private void CreateConnection()
        {
            var window = tempWindow;

            ClientName = ConfigManager.CurrentConfig.PlayerConfig.PlayerName;

            IsBusy = true;

            if (!CreateTcpConnection())
            {
                IsBusy = false;

                App.Current.Dispatcher.Invoke(() =>
                {
                    _dialogCoordinator.ShowMessageAsync(this, "Error", "Could not connect to the server.");
                });

                return;
            }

            if (SelectedClientType == ClientType.Player)
            {
                log.InfoFormat("Connecting as {0} to {1}:{2} as player.", ClientName, ServerAddress, ServerPort);
            }
            else
            {
                log.InfoFormat("Connecting as {0} to {1}:{2} as spectator.", ClientName, ServerAddress, ServerPort);
            }

            Connected = true;
            App.Current.Dispatcher.Invoke(() =>
            {
                window.Close();
            });
        }

        private bool CreateTcpConnection()
        {
            try
            {
                Client.Connect(ServerAddress, ServerPort);
                EndPoint endPoint = Client.Client.RemoteEndPoint;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
            finally
            {
                IsBusy = false;
            }

            return true;
        }
    }
}
