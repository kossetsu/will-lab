﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using MahApps.Metro.Controls.Dialogs;
using Prism.Commands;
using Prism.Mvvm;
using WillLab.Data.Config;
using WillLab.Managers;

namespace WillLab.ViewModels
{
    public class ConfigViewModel : BindableBase
    {
        // logger
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        // managers
        private IConfigManager _configManager;
        private IDialogCoordinator _dialogCoordinator;

        // commands
        public DelegateCommand SaveConfigCommand { get; private set; }

        // properties to save
        private string _playerName;

        public string PlayerName
        {
            get { return _playerName; }
            set
            {
                SetProperty(ref _playerName, value);
                SaveConfigCommand.RaiseCanExecuteChanged();
            }
        }

        public ConfigViewModel(IConfigManager cm, IDialogCoordinator dc)
        {
            _dialogCoordinator = dc;
            _configManager = cm;

            SaveConfigCommand = new DelegateCommand(ExecuteSave, CanSave);

            PopulateFields();
        }

        private void PopulateFields()
        {
            PlayerName = _configManager.CurrentConfig.PlayerConfig.PlayerName;
        }

        private void ExecuteSave()
        {
            var newConfig = new ClientConfig();

            // populate new config with field data
            newConfig.PlayerConfig.PlayerName = PlayerName;

            _configManager.SaveConfig(newConfig);

            App.Current.Dispatcher.Invoke(() =>
            {
                _dialogCoordinator.ShowMessageAsync(this, "Success", "Configuration successfully saved.");
            });
        }

        private bool CanSave()
        {
            return !string.IsNullOrEmpty(PlayerName);
        }
    }
}
