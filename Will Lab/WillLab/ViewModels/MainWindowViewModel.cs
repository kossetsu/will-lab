﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using Common.ClientResponses;
using Common.Enums;
using Common.ServerRequests;
using log4net;
using Prism.Events;
using Prism.Mvvm;
using Prism.Regions;
using WillLab.Events;
using WillLab.Managers;
using WillLab.Views;

namespace WillLab.ViewModels
{
    public class MainWindowViewModel : BindableBase
    {
        // logger
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private IEventAggregator _eventAggregator;
        private IRegionManager _regionManager;
        private IConfigManager _configManager;

        private ConnectionViewModel _connectionViewModel;
        private TcpClient _client;
        private NetworkStream _msgStream;
        private SubscriptionToken subscriptionToken;

        private Thread listenThread;
        private bool _listening;

        public MainWindowViewModel(IEventAggregator ea, IRegionManager rm, IConfigManager cm)
        {
            _eventAggregator = ea;
            _regionManager = rm;
            _configManager = cm;

            if (!Initialise())
            {
                Environment.Exit(0);
            }
            else
            {
                _client = _connectionViewModel.Client;
            }

            Connect();

            _listening = true;

            listenThread = new Thread(Listen) {IsBackground = true};
            listenThread.Start();

            ServerRequestsListener();
        }

        public bool Initialise()
        {
            var connectionView = new ConnectionView(_configManager);

            connectionView.ShowDialog();

            _connectionViewModel = (ConnectionViewModel) connectionView.DataContext;

            return _connectionViewModel.Connected;
        }

        private void Connect()
        {
            EndPoint endPoint = _client.Client.RemoteEndPoint;

            if (_client.Connected)
            {
                log.InfoFormat("Connected to the server at {0}.", endPoint);

                _msgStream = _client.GetStream();

                var msg = new UserRequest(_connectionViewModel.ClientName, _connectionViewModel.SelectedClientType);

                BinaryFormatter formatter = new BinaryFormatter();

                formatter.Serialize(_msgStream, msg);

                bool accepted = false;

                while (!accepted)
                {
                    if (_msgStream.CanRead)
                    {
                        var response = (IResponse) formatter.Deserialize(_msgStream);

                        if (response.GetResponseType() == ResponseType.FirstTimeConnection)
                        {
                            var ftcresponse = (FirstTimeConnectionResponse) response;

                            accepted = ftcresponse.ConnectionAccepted;

                            log.InfoFormat("Connection was confirmed by the server! Connected.");
                        }
                    }
                }
            }
        }

        private void Listen()
        {
            BinaryFormatter formatter = new BinaryFormatter();

            while (_listening)
            {
                if (_msgStream.CanRead)
                {
                    var response = (IResponse) formatter.Deserialize(_msgStream);
                    var type = response.GetResponseType();

                    switch (type)
                    {
                        case ResponseType.ChatMessage:
                            log.Info("Got chat message from server.");
                            _eventAggregator.GetEvent<ChatMessageEvent>().Publish((ChatMessageResponse) response);
                            break;
                    }
                }
            }
        }

        public void ServerRequestsListener()
        {
            var sendRequestEvent = _eventAggregator.GetEvent<SendRequestEvent>();

            if (subscriptionToken != null)
            {
                sendRequestEvent.Unsubscribe(subscriptionToken);
            }

            subscriptionToken = sendRequestEvent.Subscribe(SendRequestEventHandler, ThreadOption.BackgroundThread);
        }

        public void SendRequestEventHandler(IRequest request)
        {
            BinaryFormatter formatter = new BinaryFormatter();

            formatter.Serialize(_msgStream, request);

            log.DebugFormat("Request of type {0} sent to server.", request);
        }
    }
}
