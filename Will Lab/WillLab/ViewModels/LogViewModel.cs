﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.ClientResponses;
using Common.ServerRequests;
using log4net;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using WillLab.Events;

namespace WillLab.ViewModels
{
    public class LogViewModel : BindableBase
    {
        // logger
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        // view elements
        private string _messageToSend;
        private string _messages;

        // commands
        public DelegateCommand SendMessageCommand { get; private set; }

        private IEventAggregator _eventAggregator;
        private SubscriptionToken subscriptionToken;

        public string MessageToSend
        {
            get { return _messageToSend; }
            set
            {
                SetProperty(ref _messageToSend, value);
                SendMessageCommand.RaiseCanExecuteChanged();
            }
        }

        public string Messages
        {
            get { return _messages; }
            set { SetProperty(ref _messages, value); }
        }

        // end of properties

        public LogViewModel(IEventAggregator ea)
        {
            _eventAggregator = ea;
            _messages = "";

            // commands
            SendMessageCommand = new DelegateCommand(ExecuteSendChatMessage, CanSendChatMessage);

            // events
            ChatMessageListener();
        }

        private void ExecuteSendChatMessage()
        {
            var request = new ChatMessageRequest(MessageToSend);
            _eventAggregator.GetEvent<SendRequestEvent>().Publish(request);
            MessageToSend = "";
        }

        private bool CanSendChatMessage()
        {
            return !string.IsNullOrEmpty(MessageToSend);
        }

        private void AppendChatMessage(ChatMessageResponse response)
        {
            string msg;

            log.DebugFormat("Displaying chat message from {0}.", response.Sender);

            if (response.ServerMessage)
            {
                msg = String.Format("<SERVER> {0}", response.Message);
            }
            else
            {
                msg = String.Format("{0}: {1}", response.Sender, response.Message);
            }

            Messages += msg + '\n';
        }
        
        public void ChatMessageListener()
        {
            var sendRequestEvent = _eventAggregator.GetEvent<ChatMessageEvent>();

            if (subscriptionToken != null)
            {
                sendRequestEvent.Unsubscribe(subscriptionToken);
            }

            subscriptionToken = sendRequestEvent.Subscribe(AppendChatMessage, ThreadOption.BackgroundThread);
        }
    }
}
