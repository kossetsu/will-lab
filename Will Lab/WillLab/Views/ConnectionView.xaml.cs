﻿using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using WillLab.Managers;
using WillLab.ViewModels;

namespace WillLab.Views
{
    /// <summary>
    /// Interaction logic for Connection.xaml
    /// </summary>
    public partial class ConnectionView : MetroWindow
    {
        ConnectionViewModel vm = new ConnectionViewModel(DialogCoordinator.Instance);

        public ConnectionView(IConfigManager cm)
        {
            InitializeComponent();
            DataContext = vm;
            vm.ConfigManager = cm;
        }
    }
}
