﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using Unity.Attributes;
using WillLab.Managers;
using WillLab.ViewModels;

namespace WillLab.Views
{
    /// <summary>
    /// Interaction logic for ConfigView.xaml
    /// </summary>
    public partial class ConfigView : MetroWindow
    {
        private ConfigViewModel vm;

        public ConfigView(IConfigManager cm)
        {
            InitializeComponent();
            DataContext = new ConfigViewModel(cm, DialogCoordinator.Instance);
        }
    }
}
