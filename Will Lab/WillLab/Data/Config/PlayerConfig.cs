﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WillLab.Data.Config
{
    public class PlayerConfig
    {
        public string PlayerName { get; set; }

        public PlayerConfig()
        {
            // defaults
            PlayerName = "Ruler";
        }
    }
}
