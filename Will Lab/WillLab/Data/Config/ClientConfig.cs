﻿namespace WillLab.Data.Config
{
    public class ClientConfig
    {
        public PlayerConfig PlayerConfig;

        public ClientConfig()
        {
            // defaults
            PlayerConfig = new PlayerConfig();
        }
    }
}
