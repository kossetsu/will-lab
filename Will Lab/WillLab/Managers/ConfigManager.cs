﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using log4net;
using WillLab.Data.Config;

namespace WillLab.Managers
{
    public class ConfigManager : IConfigManager
    {
        // logger
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public readonly string ConfigPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Kossetsoft\\WillLab\\ClientConfig.xml";

        public ClientConfig CurrentConfig { get; set; }

        public ConfigManager()
        {
            LoadConfig();
        }
        
        public void LoadConfig()
        {
            var reader = new XmlSerializer(typeof(ClientConfig));
            
            try
            {
                var file = new StreamReader(ConfigPath);

                var loadedConfig = (ClientConfig)reader.Deserialize(file);
                file.Close();

                CurrentConfig = loadedConfig;
            }
            catch (FileNotFoundException e)
            {
                CreateDefaultConfig();
            }
        }

        public void SaveConfig(ClientConfig newConfig)
        {
            var writer = new XmlSerializer(typeof(ClientConfig));;
            var file = new StreamWriter(ConfigPath);

            writer.Serialize(file, newConfig);
            file.Close();

            LoadConfig();
        }

        private void CreateDefaultConfig()
        {
            var defaultConfig = new ClientConfig();
            
            SaveConfig(defaultConfig);
        }
    }
}
