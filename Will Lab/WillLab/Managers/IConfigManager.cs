﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WillLab.Data.Config;

namespace WillLab.Managers
{
    public interface IConfigManager
    {
        void LoadConfig();
        void SaveConfig(ClientConfig newConfig);
        
        ClientConfig CurrentConfig { get; }
    }
}
