﻿using System;
using log4net;
using MahApps.Metro.Controls;
using Prism.Ioc;
using Prism.Regions;
using Unity.Attributes;
using WillLab.ViewModels;
using WillLab.Views;

namespace WillLab
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        [Dependency]
        public MainWindowViewModel ViewModel
        {
            set { this.DataContext = value; }
        }

        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public IRegionManager RegionManager;

        public MainWindow(IRegionManager regionManager)
        {
            InitializeComponent();

            if (regionManager == null)
            {
                throw new ArgumentNullException(nameof(regionManager));
            }

            RegionManager = regionManager;

            RegionManager.RegisterViewWithRegion("LogRegion", typeof(LogView));
            RegionManager.RegisterViewWithRegion("UserActionsRegion", typeof(UserActionsView));
        }
    }
}
